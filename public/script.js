let category = ['HAPPY', 'HEARTBREAK', 'SAD', 'FROM THE VAULT', 'DEBUT', 'DANCE', '1989', 'FEARLESS', 'SPEAK NOW', 'FOLKLORE', 'EVERMORE', 'RED', 'REPUTATION', 'LOVER']; 


function displayCategory() {
  let Category = category[Math.floor(Math.random() * category.length)];
  let showCard = `${Category}`;
  document.getElementById("showing").style.background = '#7C1011';
  document.getElementById("showing").innerHTML = showCard;

}

let challenge = ['Sing the lead single of the first album you get', 'Sing your favorite song from the first album you get', 'Sing a skip', 'Sing a bridge','Name an underrated song from the first album you get', 'Sing a song the best written lyric', 'Sing the first song you come up with', 'Sing your moms favorite']; 


function displayChallenge() {
  let Challenge = challenge[Math.floor(Math.random() * challenge.length)];
  let showCard = `${Challenge}`;
  document.getElementById("showing2").style.background = '#7C1011';
  document.getElementById("showing2").innerHTML = showCard;

}
